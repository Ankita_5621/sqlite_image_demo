package com.example.sqliteimagedemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import static com.example.sqliteimagedemo.Constants.*;

public class MyDBHelper extends SQLiteOpenHelper {
    public MyDBHelper(@Nullable Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        
        db.execSQL(Constants.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


            db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);


    }

    public long insertrecord(String name, String phone, String bio,
                             String email, String dob, String addedtime, String updatedtime ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        values.put(C_NAME, name);
        values.put(C_PHONE, phone);
        values.put(C_BIO, bio);
        values.put(C_EMAIL, email);
        values.put(C_DOB, dob);
        values.put(C_ADDED_TIMESTAMP, addedtime);
        values.put(C_UPDATED_TIMESTAMP, updatedtime);

        Long id = db.insert(TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public ArrayList<ModelRecords> getAllRecords(String orderBy){
        ArrayList<ModelRecords> recordList = new ArrayList<>();
        String selectQuery = " SELECT * FROM " + Constants.TABLE_NAME + " ORDER BY " +orderBy;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()){
            do {
                ModelRecords modelRecords = new ModelRecords(
                        ""+cursor.getInt(cursor.getColumnIndex(Constants.C_ID)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_NAME)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_PHONE)),
                        ""+cursor.getString(cursor.getColumnIndex(C_BIO)),
                        ""+cursor.getString(cursor.getColumnIndex(C_EMAIL)),
                        ""+cursor.getString(cursor.getColumnIndex(C_DOB)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_IMAGE)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_ADDED_TIMESTAMP)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_UPDATED_TIMESTAMP))
                );


                recordList.add(modelRecords);
            }while (cursor.moveToNext());
        }
        db.close();

        return recordList;

    }

    public ArrayList<ModelRecords> searchRecords (String query){
        ArrayList<ModelRecords> recordList = new ArrayList<>();
        String selectQuery = " SELECT * FROM " + Constants.TABLE_NAME + " WHERE " + Constants.C_NAME + " LIKE '%" + query + " %' ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                ModelRecords modelRecords = new ModelRecords(
                        ""+cursor.getInt(cursor.getColumnIndex(Constants.C_ID)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_NAME)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_PHONE)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_EMAIL)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_DOB)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_BIO)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_IMAGE)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_ADDED_TIMESTAMP)),
                        ""+cursor.getString(cursor.getColumnIndex(Constants.C_UPDATED_TIMESTAMP)));

                recordList.add(modelRecords);
            }while (cursor.moveToNext());
        }
        db.close();
        return recordList;
    }
    public int getRecordsCount(){

        String countQuery = " SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

}
