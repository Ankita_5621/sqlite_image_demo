package com.example.sqliteimagedemo;

public class ModelRecords {
    String id;
    String name;
    String phone;
    String email;
    String dob;
    String bio;
    String image;
    String addedtime;
    String updatedtime;

    public ModelRecords(String id, String name, String phone, String email, String dob, String bio, String image, String addedtime, String updatedtime) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.dob = dob;
        this.bio = bio;
        this.image = image;
        this.addedtime = addedtime;
        this.updatedtime = updatedtime;

    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getPhone() { return phone; }

    public void setPhone(String phone) { this.phone = phone; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getDob() { return dob; }

    public void setDob(String dob) { this.dob = dob; }

    public String getBio() { return bio; }

    public void setBio(String bio) { this.bio = bio; }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }

    public String getAddedtime() { return addedtime; }

    public void setAddedtime(String addedtime) { this.addedtime = addedtime; }

    public String getUpdatedtime() { return updatedtime; }

    public void setUpdatedtime(String updatedtime) { this.updatedtime = updatedtime; }
}
