package com.example.sqliteimagedemo;

import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterRecord extends RecyclerView.Adapter<AdapterRecord.HolderRecord>{
    private Context context;
    private ArrayList<ModelRecords> recordsList;

    public AdapterRecord(Context context, ArrayList<ModelRecords> recordsList) {
        this.context = context;
        this.recordsList = recordsList;
    }

    @NonNull
    @Override
    public HolderRecord onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.show_record, parent, false);
        return new HolderRecord(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderRecord holder, int position) {
        ModelRecords model = recordsList.get(position);
        final String id = model.getId();
        String image = model.getImage();
        String name = model.getName();
        String phone = model.getPhone();
        String email = model.getEmail();
        String dob = model.getDob();
        String bio = model.getBio();
        String addedtime = model.getAddedtime();
        String updatedtime = model.getUpdatedtime();

        holder.nametv.setText(name);
        holder.phonetv.setText(phone);
        holder.emailtv.setText(email);
        holder.dobtv.setText(dob);
        holder.dobtv.setText(bio);

       /* if (image.equals("null")){
            holder.profileIv.setImageResource(R.drawable.ic_action_person);

        }else {
            holder.profileIv.setImageURI(Uri.parse(image));
        }*/


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Record_Detail.class);
                intent.putExtra("RECORD_ID", id);
                context.startActivity(intent);
            }
        });

        holder.morebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return recordsList.size();
    }

    class HolderRecord extends RecyclerView.ViewHolder {
        ImageView profileIv;
        TextView nametv, phonetv, emailtv, dobtv;
        ImageButton morebtn;
        public HolderRecord(@NonNull View itemView) {
            super(itemView);
            nametv = itemView.findViewById(R.id.showname);
            phonetv = itemView.findViewById(R.id.showphone);
            emailtv = itemView.findViewById(R.id.showemail);
            dobtv = itemView.findViewById(R.id.showdob);
            morebtn = itemView.findViewById(R.id.morebtn);
        }
    }
}
