package com.example.sqliteimagedemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.FileInputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private FloatingActionButton addbtn;
    private RecyclerView showrecords;
    private MyDBHelper dbHelper;
    private ActionBar actionBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showrecords = findViewById(R.id.records);
        dbHelper = new MyDBHelper(this);
        actionBar = getSupportActionBar();
        actionBar.setTitle("ALL RECORDS");
        loadRecords();


        showrecords = findViewById(R.id.records);


        addbtn = findViewById(R.id.addbtn);
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(MainActivity.this, Addrecord.class));

            }
        });


    }

    private void loadRecords(){
        dbHelper.getReadableDatabase();
        AdapterRecord adapterRecord = new AdapterRecord(MainActivity.this, dbHelper.getAllRecords(null));
        showrecords.setAdapter(adapterRecord);
        actionBar.setSubtitle("Total: "+dbHelper.getRecordsCount());

    }

    private void searchRecords(String query){
        AdapterRecord adapterRecord = new AdapterRecord(MainActivity.this, dbHelper.searchRecords(query));
        showrecords.setAdapter(adapterRecord);
        actionBar.setSubtitle("Total:"+dbHelper.getRecordsCount());
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadRecords();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchRecords(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchRecords(newText);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}