package com.example.sqliteimagedemo;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TextView;

import com.blogspot.atifsoftwares.circularimageview.CircularImageView;

import java.util.Calendar;
import java.util.Locale;

public class Record_Detail extends AppCompatActivity {
    private CircularImageView profileview;
    private TextView nametv, phonetv, emailtv, dobtv, biotv, addedtimetv, updatedtimetv;
    private ActionBar actionBar;
    private String recordid;
    private MyDBHelper dbHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record__detail);

        actionBar = getSupportActionBar();
        actionBar.setTitle("Record Detais");
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        recordid =  intent.getStringExtra("RECORD_ID");

        dbHelper = new MyDBHelper(this);


        profileview = findViewById(R.id.profileview);
        nametv = findViewById(R.id.nametv);
        phonetv = findViewById(R.id.phonetv);
        emailtv = findViewById(R.id.emailtv);
        dobtv = findViewById(R.id.dobtv);
        biotv = findViewById(R.id.biotv);
        addedtimetv = findViewById(R.id.addedtimetv);
        updatedtimetv = findViewById(R.id.updatedtv);

        showRecordDetails();
    }

    private void showRecordDetails() {
        String selectquery = " SELECT FROM" +Constants.TABLE_NAME+ "WHERE" +Constants.C_ID+ "=\"" +recordid+ "\"";
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectquery, null);
        if (cursor.moveToFirst()){
            do {
                String id = ""+ cursor.getInt(cursor.getColumnIndex(Constants.C_ID));
                String name = ""+ cursor.getString(cursor.getColumnIndex(Constants.C_NAME));
                String image = ""+ cursor.getString(cursor.getColumnIndex(Constants.C_IMAGE));
                String email = ""+ cursor.getString(cursor.getColumnIndex(Constants.C_EMAIL));
                String phone = ""+ cursor.getString(cursor.getColumnIndex(Constants.C_PHONE));
                String dob = ""+ cursor.getString(cursor.getColumnIndex(Constants.C_DOB));
                String bio = ""+ cursor.getString(cursor.getColumnIndex(Constants.C_BIO));
                String addedtime = ""+ cursor.getString(cursor.getColumnIndex(Constants.C_ADDED_TIMESTAMP));
                String updatedtime = ""+ cursor.getString(cursor.getColumnIndex(Constants.C_UPDATED_TIMESTAMP));

                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setTimeInMillis(Long.parseLong(addedtime));
                String timeadded = " "+ DateFormat.format("dd/MM/yyyy hh:mm:aa", calendar);

                Calendar calendar1 = Calendar.getInstance(Locale.getDefault());
                calendar.setTimeInMillis(Long.parseLong(updatedtime));
                String timeupdated = " "+ DateFormat.format("dd/MM/yyyy hh:mm:aa", calendar1);

                nametv.setText(name);
                biotv.setText(bio);
                emailtv.setText(email);
                phonetv.setText(phone);
                dobtv.setText(dob);
                addedtimetv.setText(timeadded);
                updatedtimetv.setText(timeupdated);
                if (image.equals("null")){
                    profileview.setImageResource(R.drawable.ic_action_person);

                }else {
                    profileview.setImageURI(Uri.parse(image));

                }
            }while (cursor.moveToNext());
        }
        db.close();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}